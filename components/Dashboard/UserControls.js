import React from "react";
import { Button } from "react-bootstrap";
import styles from "../../styles/Dashboard.module.css";

function UserControls() {
  return (
    <div>
      <h4>GAME INPUTS</h4>
      <Button variant="success" type="button">
        Game Image Upload
      </Button>
      <Button variant="success" type="button">
        Save Game
      </Button>
    </div>
  );
}

export default UserControls;
