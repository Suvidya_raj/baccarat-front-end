import { Nav } from "react-bootstrap";
import styles from "../../styles/SideNavbar.module.css";
import Link from "next/link";
function SideNavbar() {
  return (
    <div className={styles.navbar}>
      <Nav className={styles.navbarContainer}>
        <Link href="/" className={styles.logo}>
          <img src="/logo.png" className={styles.logo} />
        </Link>

        <Link href="/index" className={styles.link}>
          <div className={styles.links}>
            <img src="/dashboard.png" className={styles.img} />
            Dashboard
          </div>
        </Link>

        <Link href="/Logger">
          <div className={styles.links}>
            <img src="/loggerDash.png" className={styles.img} />
            Logger Dashboard
          </div>
        </Link>

        <Link href="/Logs" className={styles.links}>
          <div className={styles.links}>
            <img src="/logs.png" className={styles.img} />
            Logs
          </div>
        </Link>

        <Link href="/pattern">
          <div className={styles.links}>
            <img src="/patternsSearch.png" className={styles.img} />
            Patterns & Search
          </div>
        </Link>

        <Link href="/AutoPattern" className={styles.links}>
          <div className={styles.links}>
            <img src="/AutoPattern.png" className={styles.img} />
            Auto Pattern Recognition
          </div>
        </Link>

        <Link href="/CustomFeed" className={styles.links}>
          <div className={styles.links}>
            <img src="/CustomDataFeed.png" className={styles.img} />
            Custom Data Feed
          </div>
        </Link>

        <Link href="/TriggersAndTheories" className={styles.links}>
          <div className={styles.links}>
            <img src="/TriggersAndTheories.png" className={styles.img} />
            Triggers and Theories
          </div>
        </Link>

        <Link href="/AutoPlayer" className={styles.links}>
          <div className={styles.links}>
            <img src="/autoPlayer.png" className={styles.img} />
            Auto Player
          </div>
        </Link>

        <Link href="/AutoWage" className={styles.links}>
          <div className={styles.links}>
            <img src="/autoWage.png" className={styles.img} />
            Auto Wager Calculator
          </div>
        </Link>

        <Link href="/MultipleMA" className={styles.links}>
          <div className={styles.links}>
            <img src="/MultipleMA.png" className={styles.img} />
            Multiple Moving Average
          </div>
        </Link>

        <Link href="/WageCalculator" className={styles.links}>
          <div className={styles.links}>
            <img src="/WageCalculator.png" className={styles.img} />
            Wager Calculator
          </div>
        </Link>

        <Link href="/ShoeSeperator" className={styles.links}>
          <div className={styles.links}>
            <img src="/ShoeSeperator.png" className={styles.img} />
            Shoe Seperator
          </div>
        </Link>

        <Link href="/Statistics" className={styles.links}>
          <div className={styles.links}>
            <img src="/Statistics.png" className={styles.img} />
            Statistics
          </div>
        </Link>
      </Nav>
    </div>
  );
}

export default SideNavbar;
