import React from "react";
import SideNavbar from "./Navbar/SideNavbar";

function Layout({ children }) {
  return (
    <>
      <SideNavbar />
      {children}
    </>
  );
}

export default Layout;
